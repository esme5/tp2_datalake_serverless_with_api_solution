# ----------------------- PYTHON ------------------------

.PHONY:lint
lint:
	cd src && black -l 120 .

# ----------------------- TERRAFORM ------------------------

.PHONY:delete-terraform-repo
delete-terraform-repo:
	rm -rf terraform/.terraform/*

.PHONY:init-backend
init-backend:delete-terraform-repo
	cd terraform && terraform init

.PHONY:apply
apply:
	cd terraform && terraform apply

.PHONY:destroy
destroy:
	cd terraform && terraform destroy

.PHONY:plan
plan:select-workspace
	cd terraform && terraform plan

# ----------------------- ANSIBLE ------------------------

.PHONY: run-tests-lambda_processing
run-tests-lambda_processing:
	pytest lambda_processing/tests -vv

