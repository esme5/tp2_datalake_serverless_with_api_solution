data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

resource "aws_lambda_function" "data_processing_lambda" {
  function_name = var.data_processing_lambda_name
  filename      = data.archive_file.empty_zip_code_lambda.output_path
  description   = "Cette lambda transforme les .csv en .parquet et enrichie les timeseries avec des topologies"
  handler       = "lambda_main_app.lambda_handler"
  memory_size   = 512
  role          = aws_iam_role.iam_for_lambda.arn
  runtime       = "python3.7"
  timeout       = 900
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.data_processing_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_job_offer_bucket.arn
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_iam_role_policy_attachment" "s3_full_access" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = var.s3_full_access_policy_arn
}