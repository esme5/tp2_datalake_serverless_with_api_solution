resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket        = "${var.s3_user_bucket_name}-${var.group_number}"
  force_destroy = true
  tags = {
    Name = "esme"
  }
}
resource "aws_s3_bucket_object" "job_offers" {
  depends_on = [
  aws_s3_bucket.s3_job_offer_bucket]
  bucket = aws_s3_bucket.s3_job_offer_bucket.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.data_processing_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_bucket_object.job_offers.key
    filter_suffix       = ".csv"
  }
  depends_on = [aws_s3_bucket_object.job_offers, aws_lambda_function.data_processing_lambda]
}